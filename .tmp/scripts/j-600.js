$(document).ready(function () {
  // left: 37, up: 38, right: 39, down: 40,
  // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
  let keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
  };

  function preventDefault(e) {
    e.preventDefault();
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  } // modern Chrome requires { passive: false } when adding event


  let supportsPassive = false;

  try {
    window.addEventListener('test', null, Object.defineProperty({}, 'passive', {
      get: function () {
        supportsPassive = true;
      }
    }));
  } catch (e) {}

  let wheelOpt = supportsPassive ? {
    passive: false
  } : false;
  let wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel'; // call this to Disable

  function disableScroll() {
    window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF

    window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop

    window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile

    window.addEventListener('keydown', preventDefaultForScrollKeys, false);
  } // call this to Enable


  function enableScroll() {
    window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
    window.removeEventListener('touchmove', preventDefault, wheelOpt);
    window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
  }

  if ($('.template-j-600 .animation').length) {
    let $section = $('.template-j-600 .animation');
    let element_position_top = $section.offset().top;
    let element_position_bottom = element_position_top + $section.height();
    let timeout = 1500;
    let is_animated = false;
    let window_position = $(document).scrollTop();
    $(window).on('mousewheel touchmove DOMMouseScroll', function (event) {
      window_position = $(document).scrollTop();

      if (window_position + 150 >= element_position_top && window_position <= element_position_bottom && !is_animated) {
        disableScroll();
        $([document.documentElement, document.body]).animate({
          scrollTop: element_position_top
        }, 1000);
        setTimeout(function () {
          $section.addClass('step-2');
          setTimeout(function () {
            $section.addClass('step-3');
            setTimeout(function () {
              $section.addClass('step-4');

              if ($(document).width() <= 767) {
                enableScroll();
              } else {
                setTimeout(function () {
                  enableScroll();
                }, timeout);
              }
            }, timeout);
          }, timeout);
        }, 500);
        is_animated = true;
      }
    });
  }
});