$(document).ready(function () {
    let mainSliderThumbs = new Swiper('.main-slider-thumbs', {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesProgress: true,
    });

    let mainSlider = new Swiper('.main-slider', {
        loop: true,
        spaceBetween: 10,
        navigation: {
            nextEl: '.section-1 .slider .slider-button-next',
            // prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: mainSliderThumbs,
        },
    });

    let reviewsSlider = new Swiper('.reviews-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 50,
        navigation: {
            prevEl: '.section-8 .reviews-slider-wrapper .slider-button-prev',
            nextEl: '.section-8 .reviews-slider-wrapper .slider-button-next',
        },
        pagination: {
            el: '.section-8 .reviews-slider-wrapper .slider-pagination',
            clickable: true,
        },
        breakpoints: {
            767: {
                slidesPerView: 2,
            },
        }
    });


    let audioReviewsSlider = new Swiper('.audio-reviews-slider', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 50,
        navigation: {
            prevEl: '.section-8 .audio-reviews-slider-wrapper .slider-button-prev',
            nextEl: '.section-8 .audio-reviews-slider-wrapper .slider-button-next',
        },
        pagination: {
            el: '.section-8 .audio-reviews-slider-wrapper .slider-pagination',
            clickable: true,
        },
        breakpoints: {
            575: {
                slidesPerView: 2,
            },
        }
    });



    $('.video').click(function (e) {
        e.preventDefault();
        const href = $(this).data('href')
        const fancybox = Fancybox.show([
            {
                src: href,
                type: 'video',
            },
        ])
    })

})
